///jQuery app_more_portfolios
$(function(){

  var cpt_offset = 0;

  var nb_portfolios =  $('#more').attr('data-url2');

  $('#more').click(function(e){
    e.preventDefault();

    var url = $(this).attr('data-url');

    cpt_offset = cpt_offset + 6;
    var nb_portfolios_suppl = nb_portfolios - cpt_offset;
    if ( nb_portfolios_suppl > 6 ){ nb_portfolios_suppl = 6; }

    if ( (cpt_offset + 7) > nb_portfolios ) {
      $('#more').hide();
    }

    $.ajax({
         url: url,
         method: 'get',
         data: {
           offset: cpt_offset
         },
         success: function(reponsePHP){
           $('.liste').append(reponsePHP).find('div').slice(-nb_portfolios_suppl).hide().slideDown(500);
         },
         error: function(){
           window.alert("Problème durant la transaction...");
         }
       });
  });

});
