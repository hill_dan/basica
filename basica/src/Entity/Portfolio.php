<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation  as   Vich ;
/**
 * @ORM\Entity(repositoryClass="App\Repository\PortfolioRepository")
 * @Vich\Uploadable
 */
class Portfolio
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $client;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datecreation;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $titrefr;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $titreen;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $titresecondairefr;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $titresecondaireen;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $textefr;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $texteen;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=45)
     * @Assert\Regex(pattern="/^[a-z][a-z0-9\-]*$/")
     */
    private $slugfr;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Assert\Regex(pattern="/^[a-z][a-z0-9\-]*$/")
     */
    private $slugen;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", inversedBy="portfolios")
     */
    private $tags;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="upload_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $imageSize;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updatedAt;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
//        $this->updatedAt = new |Datetime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClient(): ?string
    {
        return $this->client;
    }

    public function setClient(string $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getDatecreation(): ?\DateTimeInterface
    {
        return $this->datecreation;
    }

    public function setDatecreation(\DateTimeInterface $datecreation): self
    {
        $this->datecreation = $datecreation;

        return $this;
    }

    public function getTitrefr(): ?string
    {
        return $this->titrefr;
    }

    public function setTitrefr(string $titrefr): self
    {
        $this->titrefr = $titrefr;

        return $this;
    }

    public function getTitreen(): ?string
    {
        return $this->titreen;
    }

    public function setTitreen(?string $titreen): self
    {
        $this->titreen = $titreen;

        return $this;
    }

    public function getTitresecondairefr(): ?string
    {
        return $this->titresecondairefr;
    }

    public function setTitresecondairefr(?string $titresecondairefr): self
    {
        $this->titresecondairefr = $titresecondairefr;

        return $this;
    }

    public function getTitresecondaireen(): ?string
    {
        return $this->titresecondaireen;
    }

    public function setTitresecondaireen(?string $titresecondaireen): self
    {
        $this->titresecondaireen = $titresecondaireen;

        return $this;
    }

    public function getTextefr(): ?string
    {
        return $this->textefr;
    }

    public function setTextefr(?string $textefr): self
    {
        $this->textefr = $textefr;

        return $this;
    }

    public function getTexteen(): ?string
    {
        return $this->texteen;
    }

    public function setTexteen(?string $texteen): self
    {
        $this->texteen = $texteen;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getSlugfr(): ?string
    {
        return $this->slugfr;
    }

    public function setSlugfr(string $slugfr): self
    {
        $this->slugfr = $slugfr;

        return $this;
    }

    public function getSlugen(): ?string
    {
        return $this->slugen;
    }

    public function setSlugen(?string $slugen): self
    {
        $this->slugen = $slugen;

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }

    /**
     * [__toString utilisé par EasyAdmin Bndle]
     * @return string [description]
     */
    public function __toString()
  	{
  		return $this->titrefr;
  	}

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    public function setImageSize(?int $imageSize): void
    {
        $this->imageSize = $imageSize;
    }

    public function getImageSize(): ?int
    {
        return $this->imageSize;
    }


}
