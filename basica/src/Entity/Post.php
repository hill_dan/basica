<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 */
class Post
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datecreation;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $titreprincipalfr;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $titreprincipalen;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $texteprincipalfr;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $texteprincipalen;

    /**
     * @ORM\Column(type="string", length=65, nullable=true)
     */
    private $titresecondairefr;

    /**
     * @ORM\Column(type="string", length=65, nullable=true)
     */
    private $titresecondaireen;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $textesecondairefr;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $textesecondaireen;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=45)
     * @Assert\Regex(pattern="/^[a-z][a-z0-9\-]*$/")
     */
    private $slugfr;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Assert\Regex(pattern="/^[a-z][a-z0-9\-]*$/")
     */
    private $slugen;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatecreation(): ?\DateTimeInterface
    {
        return $this->datecreation;
    }

    public function setDatecreation(\DateTimeInterface $datecreation): self
    {
        $this->datecreation = $datecreation;

        return $this;
    }

    public function getTitreprincipalfr(): ?string
    {
        return $this->titreprincipalfr;
    }

    public function setTitreprincipalfr(string $titreprincipalfr): self
    {
        $this->titreprincipalfr = $titreprincipalfr;

        return $this;
    }

    public function getTitreprincipalen(): ?string
    {
        return $this->titreprincipalen;
    }

    public function setTitreprincipalen(?string $titreprincipalen): self
    {
        $this->titreprincipalen = $titreprincipalen;

        return $this;
    }

    public function getTexteprincipalfr(): ?string
    {
        return $this->texteprincipalfr;
    }

    public function setTexteprincipalfr(?string $texteprincipalfr): self
    {
        $this->texteprincipalfr = $texteprincipalfr;

        return $this;
    }

    public function getTexteprincipalen(): ?string
    {
        return $this->texteprincipalen;
    }

    public function setTexteprincipalen(?string $texteprincipalen): self
    {
        $this->texteprincipalen = $texteprincipalen;

        return $this;
    }

    public function getTitresecondairefr(): ?string
    {
        return $this->titresecondairefr;
    }

    public function setTitresecondairefr(?string $titresecondairefr): self
    {
        $this->titresecondairefr = $titresecondairefr;

        return $this;
    }

    public function getTitresecondaireen(): ?string
    {
        return $this->titresecondaireen;
    }

    public function setTitresecondaireen(?string $titresecondaireen): self
    {
        $this->titresecondaireen = $titresecondaireen;

        return $this;
    }

    public function getTextesecondairefr(): ?string
    {
        return $this->textesecondairefr;
    }

    public function setTextesecondairefr(?string $textesecondairefr): self
    {
        $this->textesecondairefr = $textesecondairefr;

        return $this;
    }

    public function getTextesecondaireen(): ?string
    {
        return $this->textesecondaireen;
    }

    public function setTextesecondaireen(?string $textesecondaireen): self
    {
        $this->textesecondaireen = $textesecondaireen;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getSlugfr(): ?string
    {
        return $this->slugfr;
    }

    public function setSlugfr(string $slugfr): self
    {
        $this->slugfr = $slugfr;

        return $this;
    }

    public function getSlugen(): ?string
    {
        return $this->slugen;
    }

    public function setSlugen(?string $slugen): self
    {
        $this->slugen = $slugen;

        return $this;
    }
}
