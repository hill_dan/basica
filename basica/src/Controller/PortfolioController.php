<?php
/*
  ./src/Controller/PortfolioController.php      -      PORTFOLIOS
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Portfolio;

/* GET - POST - et autres */
use Symfony\Component\HttpFoundation\Request;

/**
 * [PortfolioController]
 *
 * Extend GeneriqueController classe personnalisée  \vendor\ieps\Core\GenericController.php - chargée par l'autoload de composer.json
 */
class PortfolioController extends GenericController  {

  /**
   * [showAction]
   * @param  int     $id      [ID]
   * @param  Request $request [utilisation de POST ou GET]
   * @return - portfolio( id, client, datecreation, titre.., titresecondaire.., texte.., image, slug.., array tags(), imageFile, imageName, imageSize, updatedAt )  .. => fr, en
   */
  public function showAction(int $id, Request $request){
    $portfolio = $this->_repository->find($id);
    return $this->render('portfolios/show.html.twig',[
        'portfolio'   => $portfolio,
        /* Envoyer un paramètre POST ou GET --- 'xxxx' => $request->query->get('xxxx') */
      ]);
  }

  /**
   * [indexAction]
   * @param  string  $vue     [index]        ->  NOTRE PORTFOLIO - page 2 - Affichage des PORTFOLIOS
   *                          [indexlast]    ->  Nos travaux récents! - Page 1 [ACCUEIL] -
   *                          [indexmore]    ->  NOTRE PORTFOLIO  -  page 2  -  PORTFOLIOS
   *                          [indexsimilar] ->  4 Travaux similaires - page Détails du portfolio
   * @param  array   $orderBy [ORDER BY]
   * @param  integer $limit   [LIMIT]
   * @param  string  $class   [ex: 'col-md-4 col-sm-6' Nomber de colonnes]
   * @param  Request $request [utilisation de POST ou GET]
   * @return - portfolios Array( portfolio( id, client, datecreation, titre.., titresecondaire.., texte.., image, slug.., array tags(), imageFile, imageName, imageSize, updatedAt ) )  .. => fr, en
   *         - int nbportfolios
   *         - string class
   */
  public function indexAction( array $orderBy = [], int $limit = 6, string $vue = 'index', string $class = 'col-md-4 col-sm-6', Request $request){

    if ( $request->query->get('vue') ){
       $vue = $request->query->get('vue');
     }

    if( $vue == 'index' ) {
      $nb_portfolios = $this->_repository->findNbPortfolios();
    }
    else {
      $nb_portfolios[1] = 0;
    }

    if ( $request->query->get('offset') ){
      $offset = $request->query->get('offset');
    }
    else { $offset = 0 ;
    }

    $portfolios = $this->_repository->findBy( [], $orderBy, $limit, $offset);

    return $this->render('portfolios/'.$vue.'.html.twig',[
        'portfolios'   => $portfolios,
        'nbportfolios' => $nb_portfolios[1],
        'class'        => $class,
        /* Envoyer un paramètre POST ou GET --- 'xxxx' => $request->query->get('xxxx') */
      ]);
  }

}
