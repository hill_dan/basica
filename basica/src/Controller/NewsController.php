<?php
/*
  ./src/Controller/NewsController.php      -      NEWS
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\News;

/* GET - POST - et autres */
use Symfony\Component\HttpFoundation\Request;

/**
 * [NewsController]
 *
 * Extend GeneriqueController classe personnalisée  \vendor\ieps\Core\GenericController.php - chargée par l'autoload de composer.json
 */
class NewsController extends GenericController  {

  /**
   * [showAction]
   * @param  int     $id      [id]
   * @param  Request $request [utilisation de POST ou GET]
   * @return - news( id, titre, texte, slug, array datecreation() )
   */
  public function showAction(int $id, Request $request){
    $news = $this->_repository->find($id);
    return $this->render('newss/show.html.twig',[
        'news'   => $news,
        /* Envoyer un paramètre POST ou GET --- 'xxxx' => $request->query->get('xxxx') */
      ]);
  }

  /**
   * [indexlastAction]
   * @param  array   $orderBy [ORDER BY]
   * @param  integer $limit   [LIMIT]
   * @return - newss Array( news( id, titre, texte, slug, array datecreation() ) )
   */
  public function indexlastAction( array $orderBy = [], int $limit = 3 ){
    $newss = $this->_repository->findBy( [], $orderBy, $limit );	/* Remplacer tri par le nom du champ à trier  */
																	/* OU  $newss = $this->_repository->findAll(); */
    return $this->render('newss/indexlast.html.twig',[
        'newss'   => $newss
        /* Envoyer un paramètre POST ou GET --- 'xxxx' => $request->query->get('xxxx') */
      ]);
  }

}
