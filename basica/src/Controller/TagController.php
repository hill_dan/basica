<?php
/*
  ./src/Controller/TagController.php      -      TAGS
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Tag;

/* GET - POST - et autres */
use Symfony\Component\HttpFoundation\Request;

/**
 * [TagController]
 *
 * Extend GeneriqueController classe personnalisée  \vendor\ieps\Core\GenericController.php - chargée par l'autoload de composer.json
 */
class TagController extends GenericController  {

  /**
   * [showAction]
   * @param  int     $id      [ID]
   * @param  Request $request [utilisation de POST ou GET]
   * @return - tag( id, nom.., slug.., array portfolios() )           .. => fr, en
   */
  public function showAction(int $id, Request $request){
    $tag = $this->_repository->find($id);
    return $this->render('tags/show.html.twig',[
        'tag'   => $tag,
        /* Envoyer un paramètre POST ou GET --- 'xxxx' => $request->query->get('xxxx') */
      ]);
  }

  /**
   * [indexAction] - Liste des TAGS
   * @return - tags Array(tag( id, nom.., slug.., array portfolios() ))  .. => fr, en
   */
  public function indexAction(){
    $tags = $this->_repository->findAll();	/* Remplacer tri par le nom du champ à trier  */
																	/* OU  $tags = $this->_repository->findAll(); */
    return $this->render('tags/index.html.twig',[
        'tags'   => $tags,
        /* Envoyer un paramètre POST ou GET --- 'xxxx' => $request->query->get('xxxx') */
      ]);
  }

}
