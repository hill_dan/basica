<?php
/*
  ./src/Controller/PostController.php      -      POSTS
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Post;

/* GET - POST - et autres */
use Symfony\Component\HttpFoundation\Request;

/**
 * [PostController]
 *
 * Extend GeneriqueController classe personnalisée  \vendor\ieps\Core\GenericController.php - chargée par l'autoload de composer.json
 */
class PostController extends GenericController  {

  /**
   * POST - ShowACTION
   * @param  int     $id      [id]
   * @param  Request $request [utilisation de POST ou GET]
   * @return - post( id datecreation titreprincipalfr titreprincipalen texteprincipalfr texteprincipalen titresecondairefr titresecondaireen textesecondairefr textesecondaireen image slugfr slugen )
   */
  public function showAction(int $id, Request $request){
    $post = $this->_repository->find($id);
    return $this->render('posts/show.html.twig',[
        'post'   => $post,
        /* Envoyer un paramètre POST ou GET --- 'xxxx' => $request->query->get('xxxx') */
      ]);
  }

  /**
   * POST - IndexACTION
   * @param  string  $vue      [index]     -> Liste des blogs - page 3 [BLOGS]
   *                           [indexlast] -> Derniers articles de blog - page 1
   *                           [indexlist] -> Liste des post dans l'Aside
   * @param  array   $criteria [critères de recherche]
   * @param  array   $orderBy  [ordre]
   * @param  integer $limit    [limit]
   * @param  integer $offset   [offset]
   * @return    - posts array( post( id datecreation titreprincipalfr titreprincipalen texteprincipalfr texteprincipalen titresecondairefr titresecondaireen textesecondairefr textesecondaireen image slugfr slugen ))
   *            - limit
   *            - offset
   *            - nbposts
   */
  public function indexAction( string $vue = 'index', array $criteria = [], array $orderBy = ['id' => 'ASC'], int $limit = 4, int $offset = 0 ){
    $posts = $this->_repository->findBy($criteria, $orderBy, $limit, $offset);
    $nbposts = $this->_repository->findNbPost();

    return $this->render('posts/'.$vue.'.html.twig',[
        'posts'   => $posts,
        'limit' => $limit,
        'offset' => $offset,
        'nbposts' => $nbposts
      ]);
  }

}
