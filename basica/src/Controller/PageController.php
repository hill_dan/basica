<?php
/*
  ./src/Controller/PageController.php      -      PAGES
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Page;

/* GET - POST - et autres */
use Symfony\Component\HttpFoundation\Request;

/**
 * [PageController]
 *
 * Extend GeneriqueController classe personnalisée  \vendor\ieps\Core\GenericController.php - chargée par l'autoload de composer.json
 */
class PageController extends GenericController  {

  /**
   * [showAction]
   * @param  int     $id      [ID]
   * @param  Request $request [utilisation de POST ou GET]
   * @return - page(id titrefr titreen textefr texteen slugfr slugen tri)
   *         - sqloffset
   */
  public function showAction(int $id, Request $request){
    $page = $this->_repository->find($id);
    return $this->render('pages/show.html.twig',[
        'page'   => $page,
        'sqloffset' => $request->query->get('offset')
      ]);
  }

  /**
   * [indexAction]
   * @param  integer $numPage [Numero de la page en cours]
   * @return - pages Array( Page( id, titre..,texte.., slug.., tri ) )   Multilingue  .. => fr, en
   *         - numPage int
   */
  public function indexAction( int $numPage = 0 ){
    $pages = $this->_repository->findBy([], ['tri' => 'ASC'] );
    return $this->render('pages/index.html.twig',[
        'pages'   => $pages,
        'numPage' => $numPage
      ]);
  }

}
