<?php
/*
  ./src/Controller/SlideController.php      -      SLIDES
*/
namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Slide;

/* GET - POST - et autres */
use Symfony\Component\HttpFoundation\Request;

/**
 * [SlideController]
 *
 * Extend GeneriqueController classe personnalisée  \vendor\ieps\Core\GenericController.php - chargée par l'autoload de composer.json
 */
class SlideController extends GenericController  {

  /**
   * [showAction] - non utilisé
   * @param  int     $id      [ID]
   * @param  Request $request [utilisation de POST ou GET]
   * @return - slide( id, titre.., texte.., lien, image, slug.. ) Multilingue titre.. -> titrefr titreen
   */
  public function showAction(int $id, Request $request){
    $slide = $this->_repository->find($id);
    return $this->render('slides/show.html.twig',[
        'slide'   => $slide,
        /* Envoyer un paramètre POST ou GET --- 'xxxx' => $request->query->get('xxxx') */
      ]);
  }

  /**
   * [indexAction] - Affichage du SLIDE
   * @return - slides Array(slide( id, titre.., texte.., lien, image, slug.. )) Multilingue titre.. -> titrefr titreen
   */
  public function indexAction(){
    $slides = $this->_repository->findAll();
    return $this->render('slides/index.html.twig',[
        'slides'   => $slides
      ]);
  }

}
