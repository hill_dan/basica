<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190420160242 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE page (id INT AUTO_INCREMENT NOT NULL, titrefr VARCHAR(45) NOT NULL, titreen VARCHAR(45) DEFAULT NULL, textefr LONGTEXT DEFAULT NULL, texteen LONGTEXT DEFAULT NULL, slugfr VARCHAR(45) NOT NULL, slugen VARCHAR(45) DEFAULT NULL, tri INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE portfolio (id INT AUTO_INCREMENT NOT NULL, client VARCHAR(45) NOT NULL, datecreation DATETIME NOT NULL, titrefr VARCHAR(45) NOT NULL, titreen VARCHAR(45) DEFAULT NULL, titresecondairefr VARCHAR(45) DEFAULT NULL, titresecondaireen VARCHAR(45) DEFAULT NULL, textefr LONGTEXT DEFAULT NULL, texteen LONGTEXT DEFAULT NULL, image VARCHAR(45) NOT NULL, slugfr VARCHAR(45) NOT NULL, slugen VARCHAR(45) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE portfolio_tag (portfolio_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_8731A46CB96B5643 (portfolio_id), INDEX IDX_8731A46CBAD26311 (tag_id), PRIMARY KEY(portfolio_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE post (id INT AUTO_INCREMENT NOT NULL, datecreation DATETIME NOT NULL, titreprincipalfr VARCHAR(45) NOT NULL, titreprincipalen VARCHAR(45) DEFAULT NULL, texteprincipalfr LONGTEXT DEFAULT NULL, texteprincipalen LONGTEXT DEFAULT NULL, titresecondairefr VARCHAR(65) DEFAULT NULL, titresecondaireen VARCHAR(65) DEFAULT NULL, textesecondairefr LONGTEXT DEFAULT NULL, textesecondaireen LONGTEXT DEFAULT NULL, image VARCHAR(45) DEFAULT NULL, slugfr VARCHAR(45) NOT NULL, slugen VARCHAR(45) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE slide (id INT AUTO_INCREMENT NOT NULL, titrefr VARCHAR(55) NOT NULL, titreen VARCHAR(55) DEFAULT NULL, textefr LONGTEXT DEFAULT NULL, texteen LONGTEXT DEFAULT NULL, lien VARCHAR(45) DEFAULT NULL, image VARCHAR(45) NOT NULL, slugfr VARCHAR(45) NOT NULL, slugen VARCHAR(45) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag (id INT AUTO_INCREMENT NOT NULL, nomfr VARCHAR(45) NOT NULL, nomen VARCHAR(45) DEFAULT NULL, slugfr VARCHAR(45) NOT NULL, slugen VARCHAR(45) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE portfolio_tag ADD CONSTRAINT FK_8731A46CB96B5643 FOREIGN KEY (portfolio_id) REFERENCES portfolio (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE portfolio_tag ADD CONSTRAINT FK_8731A46CBAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE portfolio_tag DROP FOREIGN KEY FK_8731A46CB96B5643');
        $this->addSql('ALTER TABLE portfolio_tag DROP FOREIGN KEY FK_8731A46CBAD26311');
        $this->addSql('DROP TABLE page');
        $this->addSql('DROP TABLE portfolio');
        $this->addSql('DROP TABLE portfolio_tag');
        $this->addSql('DROP TABLE post');
        $this->addSql('DROP TABLE slide');
        $this->addSql('DROP TABLE tag');
    }
}
